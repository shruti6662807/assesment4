package cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class app {

    @Given("I have a sample feature file")
    public void givenStep() {
        System.out.println("Step 1: Given I have a sample feature file");
    }

    @When("I run the feature file")
    public void whenStep() {
        System.out.println("Step 2: When I run the feature file");
    }

    @Then("I should see output")
    public void thenStep() {
        System.out.println("Step 3: Then I should see output");
    }
}

